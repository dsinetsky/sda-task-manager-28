package ru.t1.dsinetsky.tm.api.component;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    String getPasswordSecret();

    int getPasswordIteration();

}
