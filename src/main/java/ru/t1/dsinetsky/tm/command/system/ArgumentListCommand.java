package ru.t1.dsinetsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.model.ICommand;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = ArgumentConst.CMD_ARG;

    @NotNull
    public static final String NAME = TerminalConst.CMD_ARG;

    @NotNull
    public static final String DESCRIPTION = "Shows list of arguments";

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            if (command.getArgument() != null && !command.getArgument().isEmpty())
                System.out.println(command.getArgument());
        }
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
