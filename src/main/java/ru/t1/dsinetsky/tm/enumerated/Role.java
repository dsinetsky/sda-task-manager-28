package ru.t1.dsinetsky.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.system.InvalidRoleException;

@Getter
public enum Role {

    ADMIN("Administrator"),
    USUAL("User");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) throws InvalidRoleException {
        if (value == null || value.isEmpty()) throw new InvalidRoleException();
        for (@NotNull final Role role : values())
            if (value.equals(role.name())) return role;
        throw new InvalidRoleException();
    }

}
